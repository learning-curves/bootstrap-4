# Bootstrap 4 Alpha/Beta hidden-*-up/down migration

**Problem:** In Bootstrap 4 [alpha](https://v4-alpha.getbootstrap.com/layout/responsive-utilities/#available-classes) there were responsive utility classes named `.hidden-*-up`, `.hidden-*-down`, that were removed in BS4 [beta](https://getbootstrap.com/docs/4.0/utilities/display/).

## Quick Links
- [Video Explanation](https://youtu.be/UG7I7c12d-Y)
- [Codepen Examples](https://codepen.io/devwax/pen/RZMGPo)

## Alpha examples
```html
<div class="hidden-md-up"></div>

<div class="hidden-md-down"></div>

<span class="hidden-sm-down"></span>
```

Bootstrap 4 Beta removes these utility classes in favor of a more fine-grained responsive display utilities, like `d-none`, `d-*-block`. See: [migration notice](https://getbootstrap.com/docs/4.0/migration/#utilities), and new [responsive display utilities docs](https://getbootstrap.com/docs/4.0/utilities/display/) for more.

## Beta Examples
*Equivalent of first examples:*
```html
<div class="d-md-none">
<!--
  - Equivalent to: .hidden-md-up
  - display:none md and up.
  - Default mobile-first behaviour is display:block at xs and up
-->

<div class="d-none d-lg-block">
<!--
  - Equivalent to: .hidden-md-down
  - display:none md and down
  - lg is one breakpoint higher than md
-->

<span class="d-none d-md-inline">
<!--
  - Equivalent to: .hidden-sm-down
  - display:none below md (sm-down)
  - display:inline md and up (equivalent to hidden-sm-down)
-->
```

This is more inline *(no pun)*, with mobile first philosophy.

Also, remember that breakpoints referenced *(sm, md, lg, etc)* implicitly mean *"and up"*. So, `.d-none.d-md-block` means `display:block` @ `md` breakpoint __*and up*__; not just `md`. Otherwise, one would need to set breakpoints for *.d-lg-block, .d-xl-block* explicitly, which is not necessary when adhering to a mobile first design pattern.

### Resources:

[Codepen Examples](https://codepen.io/devwax/pen/RZMGPo)


**Official Docs**

[Responsive display utilities (.d-none.d-md-block, etc)](https://getbootstrap.com/docs/4.0/utilities/display/#common-display-values)

[Official Migration Notes (responsive utilities to bs4-beta)](https://getbootstrap.com/docs/4.0/migration/#utilities)

**Github issue comments:**

[Explanation and migration table](https://github.com/twbs/bootstrap/issues/23475#issue-250426778) - [by Joyrex](https://github.com/Joyrex)

[@mdo](https://github.com/mdo)'s [comment confirming and crystalizing the issue](https://github.com/twbs/bootstrap/issues/23475#issuecomment-322667498).
