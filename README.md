# Learning Bootstrap 4 Beta

This is a repository for my experiments with Bootstrap 4.

## Docs & Resources

- [Bootstrap 4 Alpha/Beta hidden-*-up/down migration](https://gitlab.com/learning-curves/bootstrap-4/blob/master/responsive-display-utilities.md)
  - An explanation of Bootstrap 4 Beta's retooling of their responsive utilities with migration examples.
